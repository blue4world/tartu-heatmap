  <div id="home" data-role="page">
    <div data-role="header" data-position="fixed">
      <h3>Life Quality Map Tartu </h3> 
        <a id="save_btn"  class="ui-btn ui-btn-right">Save</a>
        <a id="cancel_btn"  class="ui-btn ui-btn-left">Cancel</a>
    </div>
        <?php include "form_basic.html" ?>
        <?php include "form_expert.php" ?>
        <?php include "form_statistics.php" ?>
    <div data-position="fixed" data-role="footer">
      <div data-role="navbar">
        <ul>
          <li>
            <a id="basic_btn" data-icon="edit" class="ui-btn-active">Basic</a>
          </li>
          <li>
            <a id="expert_btn" data-icon="user">Expert</a>
          </li>
          <li>
            <a id="stat_btn" data-icon="eye">Statistics</a>
          </li>
        </ul>
      </div>
    </div>
  </div>

<script>
function showExpert() {
     console.log("showExpert");        
    $("#form_expert").css('display', 'block');
    $("#form_basic").css('display', 'none');
    $("#form_statistics").css('display', 'none');
}

function showBasic() {
     console.log("showBasic");        
    $("#form_expert").css('display', 'none');
    $("#form_basic").css('display', 'block');
    $("#form_statistics").css('display', 'none');
}


function showStatistics() {
     console.log("showStatistic");        
    $("#form_expert").css('display', 'none');
    $("#form_basic").css('display', 'none');
    $("#form_statistics").css('display', 'block');
}

$(document).ready(function() {
    $("#expert_btn").click(function(){
     console.log("expert button pressed");        
     showExpert();
     scrollTop();
    });

    $("#basic_btn").click(function(){
     console.log("basic button pressed");        
        showBasic();
        scrollTop();
    }); 

    $("#stat_btn").click(function(){
     console.log("stat button pressed");        
        showStatistics();
        scrollTop();
    }); 

});

</script>
