    <div id="form_statistics" role="main" class="ui-content" style="padding: 10%;">
        <h1 is="gk-text">Statistics</h1>
        <div id="histogram" class="ui-content"> </div> <br/>
        <div id="box_plot" class="ui-content"> </div> <br/>
        <a id="down_btn"  class="ui-btn">Download data</a>
        <div id="expert_text" class="ui-content"> </div> 
    </div>

