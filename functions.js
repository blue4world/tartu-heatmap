function processData(data) {
var fitness = [];
for (var i = 0; i < data.rows.length; i++) {
                fitness.push(data.rows[i]["fitness"]);
        }
        for (var i = 0; i < fitness.length; i++) {
                if(fitness[i] < 0){
                      console.log("Somethinng went wrong! Less than zero");
                      fitness[i]=0;
                }
        }
return fitness;
}

function calculateSqlList(){
sql_list = "("+
atm+"*atm+"+
bar_cafe+"*bar_cafe+"+
bus_stop+"*bus_stop+"+
c_parking+"*c_parking+"+
culture+"*culture+"+
fast_food+"*fast_food+"+
fire_polic+"*fire_polic+"+
hospital+"*hospital+"+
kindergard+"*kindergard+"+
library+"*library+"+
market+"*market+"+
museum+"*museum+"+
pharmacy+"*pharmacy+"+
playground+"*playground+"+
post_box+"*post_box+"+
restaurant+"*restaurant+"+
school+"*school+"+
shops+"*shops+"+
sport+"*sport+"+
u_bussines+"*u_bussines+"+
u_computer+"*u_computer+"+
u_law+"*u_law+"+
u_medicine+"*u_medicine+"+
u_philosop+"*u_philosop+"+
u_science+"*u_science+"+
u_sport+"*u_sport+"+
waste+"*waste+"+
wood+"*wood)";
}

function medianValue(values) {
var half = Math.floor(values.length/2);
if(values.length % 2)
return values[half];
else
return (values[half-1] + values[half]) / 2.0;
}

function quantile1(values) {
var q1 = Math.floor(values.length/3);
if(values.length % 3)
return values[q1];
else
return (values[q1-1] + values[q1]) / 2.0;
}

function quantile3(values) {
var q3 = Math.floor(values.length/3);
q3=q3*2;
if(values.length % 3)
return values[q3];
else
return (values[q3-1] + values[q3]) / 2.0;
}


function segment(values,segment) {
//console.log("Total lenght:");
//console.log(values.length)
var lenght = Math.floor(values.length/18);
lenght = segment*lenght;

//console.log("segment");
//console.log(segment);
//console.log("value")
//console.log(lenght);

if(values.length % 18)
return values[lenght];
else
return (values[lenght-1] + values[lenght]) / 2.0;
}


function getMax(){
// max
      var sql_max = "SELECT max("+sql_list+") FROM tartu_buildings"
        var sql = new cartodb.SQL({ user: 'blue4world' });
        sql.execute(sql_max)
        .done(function(data) {
        maximal=data.rows[0];
        maximal=maximal["max"];
        console.log("maximal value");
        console.log(maximal);
        getMin();
        })
        .error(function(errors) {
        console.log("errors:" + errors);
        })
        return maximal;
}

function getMin(){
// min
        var sql_min = "SELECT min("+sql_list+") FROM tartu_buildings"
        var sql = new cartodb.SQL({ user: 'blue4world' });
        sql.execute(sql_min)
        .done(function(data) {
        minimal=data.rows[0];
        minimal=minimal["min"];
        console.log("minimal value");
        console.log(minimal);
        getFitness();
        })
        .error(function(errors) {
        console.log("errors:" + errors);
        })
        return minimal;
}

function statistics(){
console.log("--STATISTIC--");

console.log("max:");        
console.log(maximal);       

console.log("min:");        
console.log(minimal);       

console.log("median:");
console.log(median);

console.log("quantile1:");
console.log(q1);

console.log("quantile3:");
console.log(q3);

}


function plotHistogram(){
var histData = [
  {
    x: fitness,
    type: 'histogram'
  }
];
Plotly.newPlot('histogram', histData);
}

function plotBoxGraph(){
var boxPlotData1 = {
  x: fitness,
  type: 'box',
  autosize: true,
  name: 'Mean and Standard Deviation',
  marker: {
    color: 'rgb(10,140,208)'
  },
  boxmean: 'sd'
};

var boxPlotData = [boxPlotData1];

var layout = {
  title: 'Box Plot'
};
Plotly.newPlot('box_plot', boxPlotData,layout);
}


function drawExpertInfo(){
var string="<b>Data count: </b>"+fitness.length+"<br/>" +
"<b>Min: </b>"+minimal+"<br/>"+
"<b>Max: </b>"+maximal+"<br/>"+
"<b>Median: </b>"+median+"<br/>"+
"<b>Quantile1: </b>"+q1+"<br/>"+
"<b>Limit: </b>"+limit+" %<br/>"+
"<b>Quantile3: </b>"+q3+"<br/>";
string=string+"<b>Borders: </b><br/>";
for(var i=0;i<m.length;i++) {
string=string+m[i]+"<br/>";
}
string=string+'<b>SQL: </b><br/><pre><code><div style="word-break:break-all;">'+sql_q+"</div></code></pre><br/>";
$("#expert_text").html(string);
}

function scrollTop(){
     $("html, body").animate({ scrollTop: 0 }, "slow");
}



