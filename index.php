<html>
<head>
<title>Tartu Heatmap</title>
<link rel="shortcut icon" href="https://www.is.ut.ee/favicon.ico" type="image/x-icon">
<script src="plotly.js"></script>
<?php include("include.html"); ?>
<script src="global.js"></script>
<script src="calculateFromBasic.js"></script>
<script src="functions.js"></script>
<script src="js_form_functions.js"></script>
<script src="my_cartodb.js"></script>


  <style>
  html, body {width:100%; height:100%; padding: 0; margin: 0;}
   #cartodb-map { width: 100%; height:100%; background: black;}
  </style>


</head>

<body onload="init()">

<?php include("form.php"); ?>


  <div id='cartodb-map'></div>

<?php include("legend.html"); ?>


<script src="js_form.js"></script>


</body>
</html>
