function showForm() {
     console.log("showForm");        
     $("#home").css('visibility', 'visible');
    $("#cartodb-map").css('visibility', 'hidden');
    $("#legend").css('visibility', 'hidden');
     $("#form_expert").css('display', 'none');
     $("#form_statistics").css('display', 'none');
     $("#form_basic").css('display', 'block');

}

function hideForm() {
     console.log("hideForm");        
    $("#home").css('visibility', 'hidden');
    $("#cartodb-map").css('visibility', 'visible');
    $("#legend").css('visibility', 'visible');

    $("#form_expert").css('display', 'none');
    $("#form_basic").css('display', 'none');

     $("#basic_btn").addClass("ui-btn-active");
     $("#expert_btn").removeClass("ui-btn-active");
     $("#stat_btn").removeClass("ui-btn-active");
}

function cancelForm(){
     console.log("cancel button pressed");        
     $("html, body").animate({ scrollTop: 0 }, "slow");
     hideForm();   
}



$(document).ready(function() {
    $("#choose_btn").click(function(){
     console.log("choose button pressed");        
     showForm();
     loadDataToExpertFields();
     loadDataToBasicFields();
    $("#save_btn").click(function(){
     console.log("save button pressed");        
        scrollTop();
        handleCityArea();
       if( $('#form_basic').css('display') == "block") {
                     console.log("applying basic");        
                     handleDataBasic();
                     calculateDataFromBasic();
                             hideForm();   
                             init();
        }
        else if ( $('#form_expert').css('display') == "block") {
                     console.log("applying expert");       
                     handleDataExpert();
                             hideForm();   
                             init();
       }
       else {
                cancelForm();
        }
     

    }); 
    $("#cancel_btn").click(function(){
        cancelForm();
    }); 
    $("#reset_btn").click(function(){
        resetExpert();
    }); 
    $("#down_btn").click(function(){
                console.log("down_btn");
                window.location.href = "https://blue4world.cartodb.com/tables/tartu_buildings_f/public/map";
    }); 
    }); 

        $( "#limit" ).change(function() {
                limit= $('#limit').val();
                init();
        });


        $( "#scale-1" ).change(function() {
                if($('#scale-1').is(':checked')) { 
                                legend_fixed=1;
                                init();
                 }
                else {
                                legend_fixed=0;
                                init();
                }

        });

        $( "#scale-2" ).change(function() {
                if($('#scale-2').is(':checked')) { 
                                legend_fixed=0;
                                init();
                 }
                else {
                                legend_fixed=1;
                                init();
                }
        });

});

hideForm();



