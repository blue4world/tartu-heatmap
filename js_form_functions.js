function loadDataToExpertFields() {
$("#area-1").removeAttr("checked");
$("#area-2").removeAttr("checked");
$("#area-3").removeAttr("checked");

if(area=="area_all"){
$("#area-1").prop('checked', true);
 }
if(area=="area_2"){
$("#area-2").prop('checked', true);
 }
if(area=="area_1"){
$("#area-3").prop('checked', true);
 }

$("#area-1").checkboxradio("refresh");
$("#area-2").checkboxradio("refresh");
$("#area-3").checkboxradio("refresh");

$("#atm").val(Math.floor(atm*100));
$('#bar_cafe').val(Math.floor(bar_cafe*100));
$('#bus_stop').val(Math.floor(bus_stop*100));
$('#c_parking').val(Math.floor(c_parking*100));
$('#culture').val(Math.floor(culture*100));
$('#fast_food').val(Math.floor(fast_food*100));
$('#fire_polic').val(Math.floor(fire_polic*100));
$('#hospital').val(Math.floor(hospital*100));
$('#kindergard').val(Math.floor(kindergard*100));
$('#library').val(Math.floor(library*100));
$('#market').val(Math.floor(market*100));
$('#museum').val(Math.floor(museum*100));
$('#pharmacy').val(Math.floor(pharmacy*100));
$('#playground').val(Math.floor(playground*100));
$('#post_box').val(Math.floor(post_box*100));
$('#restaurant').val(Math.floor(restaurant*100));
$('#school').val(Math.floor(school*100));
$('#shops').val(Math.floor(shops*100));
$('#sport').val(Math.floor(sport*100));
$('#u_bussines').val(Math.floor(u_bussines*100));
$('#u_computer').val(Math.floor(u_computer*100));
$('#u_law').val(Math.floor(u_law*100));
$('#u_medicine').val(Math.floor(u_medicine*100));
$('#u_philosop').val(Math.floor(u_philosop*100));
$('#u_science').val(Math.floor(u_science*100));
$('#u_sport').val(Math.floor(u_sport*100));
$('#waste').val(Math.floor(waste*100));
$('#wood').val(Math.floor(wood*100));
}

function resetExpert(){
$("#atm").val(0);
$('#bar_cafe').val(0);
$('#bus_stop').val(0);
$('#c_parking').val(0);
$('#culture').val(0);
$('#fast_food').val(0);
$('#fire_polic').val(0);
$('#hospital').val(0);
$('#kindergard').val(0);
$('#library').val(0);
$('#market').val(0);
$('#museum').val(0);
$('#pharmacy').val(0);
$('#playground').val(0);
$('#post_box').val(0);
$('#restaurant').val(0);
$('#school').val(0);
$('#shops').val(0);
$('#sport').val(0);
$('#u_bussines').val(0);
$('#u_computer').val(0);
$('#u_law').val(0);
$('#u_medicine').val(0);
$('#u_philosop').val(0);
$('#u_science').val(0);
$('#u_sport').val(0);
$('#waste').val(0);
$('#wood').val(0);
}

function loadDataToBasicFields() {
$("#area2-1").removeAttr("checked");
$("#area2-2").removeAttr("checked");
$("#area2-3").removeAttr("checked");
if(area=="area_all"){
$("#area2-1").prop('checked', true);
 }
if(area=="area_2"){
$("#area2-2").prop('checked', true);
 }
if(area=="area_1"){
$("#area2-3").prop('checked', true);
 }
$("#area2-1").checkboxradio("refresh");
$("#area2-2").checkboxradio("refresh");
$("#area2-3").checkboxradio("refresh");


$("#radio-choice-1").removeAttr("checked");
$("#radio-choice-2").removeAttr("checked");
$("#radio-choice-3").removeAttr("checked");
if(b_age==1){
$("#radio-choice-1").prop('checked', true);
 }
if(b_age==2){
$("#radio-choice-2").prop('checked', true);
 }
if(b_age==3){
$("#radio-choice-3").prop('checked', true);
 }
$("#radio-choice-1").checkboxradio("refresh");
$("#radio-choice-2").checkboxradio("refresh");
$("#radio-choice-3").checkboxradio("refresh");


$("#public_transportation").val(b_public_transportation);

$("#radio-choice-4").removeAttr("checked");
$("#radio-choice-5").removeAttr("checked");
if(b_car==1){
$("#radio-choice-4").prop('checked', true);
 }
if(b_car==0){
$("#radio-choice-5").prop('checked', true);
 }
$("#radio-choice-4").checkboxradio("refresh");
$("#radio-choice-5").checkboxradio("refresh");

$("#cultural_life").val(b_cultural_life);
$("#sport_activities").val(b_sport_activities);

$("#radio-choice-6").removeAttr("checked");
$("#radio-choice-7").removeAttr("checked");
if(b_kids==1){
$("#radio-choice-6").prop('checked', true);
 }
if(b_kids==0){
$("#radio-choice-7").prop('checked', true);
 }
$("#radio-choice-6").checkboxradio("refresh");
$("#radio-choice-7").checkboxradio("refresh");


}


function handleDataBasic() {
//        console.log("-- Data from basic --");
        // age        
        if($('#radio-choice-1').is(':checked')) { b_age="1"; }
        if($('#radio-choice-2').is(':checked')) { b_age="2"; }
        if($('#radio-choice-3').is(':checked')) { b_age="3"; }
//        console.log("age:");        
//        console.log(b_age);
        // faculty
        b_faculty = $('#faculty_selector').find(":selected").val();
//        console.log("faculty");
//        console.log(b_faculty);
        // public trantation
        b_public_transportation= $('#public_transportation').val();
//        console.log("public_transportation:");        
//        console.log(b_public_transportation);
        // car
        if($('#radio-choice-4').is(':checked')) { b_car="1"; }
        if($('#radio-choice-5').is(':checked')) { b_car="0"; }
//        console.log("car:");        
//        console.log(b_car);
        // social life
        b_social_life= $('#social_life').val();
//        console.log("social_life:");        
//        console.log(b_social_life);
        // cultural life
        b_cultural_life= $('#cultural_life').val();
//        console.log("cultural_life:");        
//        console.log(b_cultural_life);
        // sport
        b_sport_activities= $('#sport_activities').val();
//        console.log("sport_activities:");        
//        console.log(b_sport_activities);
        // kids
        if($('#radio-choice-6').is(':checked')) { b_kids="1"; }
        if($('#radio-choice-7').is(':checked')) { b_kids="0"; }
//        console.log("kids:");        
//        console.log(b_kids);
}


function handleCityArea(){

if( $('#form_expert').css('display') == "none") {
if($('#area2-1').is(':checked')) { area="area_all"; }
if($('#area2-2').is(':checked')) { area="area_2"; }
if($('#area2-3').is(':checked')) { area="area_1"; }
}
else {
if($('#area-1').is(':checked')) { area="area_all"; }
if($('#area-2').is(':checked')) { area="area_2"; }
if($('#area-3').is(':checked')) { area="area_1"; }
}

}

function handleDataExpert() {
atm= $('#atm').val()/100.0;
bar_cafe=$('#bar_cafe').val()/100.0;
bus_stop=$('#bus_stop').val()/100.0;
c_parking=$('#c_parking').val()/100.0;
culture=$('#culture').val()/100.0;
fast_food=$('#fast_food').val()/100.0;
fire_polic=$('#fire_polic').val()/100.0;
hospital=$('#hospital').val()/100.0;
kindergard =$('#kindergard').val()/100.0;
library=$('#library').val()/100.0;
market=$('#market').val()/100.0;
museum=$('#museum').val()/100.0;
pharmacy=$('#pharmacy').val()/100.0;
playground=$('#playground').val()/100.0;
post_box=$('#post_box').val()/100.0;
restaurant=$('#restaurant').val()/100.0;
school=$('#school').val()/100.0;
shops=$('#shops').val()/100.0;
sport=$('#sport').val()/100.0;
u_bussines=$('#u_bussines').val()/100.0;
u_computer=$('#u_computer').val()/100.0;
u_law=$('#u_law').val()/100.0;
u_medicine=$('#u_medicine').val()/100.0;
u_philosop=$('#u_philosop').val()/100.0;
u_science=$('#u_science').val()/100.0;
u_sport=$('#u_sport').val()/100.0;
waste=$('#waste').val()/100.0;
wood=$('#wood').val()/100.0;
}

