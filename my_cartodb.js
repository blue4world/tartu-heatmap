function generateCSSfixed(){
var legend_diff=(maximal-minimal)/17.0;
m = [];
m.push(minimal);
for (var i = 1; i <= 16; i++) {
m.push((minimal+legend_diff*i));
}
m.push(maximal);

//console.log(m);

css = "#tartu_buildings{   polygon-fill: #ff0000;   polygon-opacity: 0.7;   line-color: #FFF;   line-width: 0.5;   line-opacity: 1; }  "+
"#tartu_buildings [ fitness <= "+m[17]+"] {    polygon-fill: #ff0000; }  "+
"#tartu_buildings [ fitness <= "+m[16]+"] {    polygon-fill: #ff2b00; }  "+
"#tartu_buildings [ fitness <= "+m[15]+"] {    polygon-fill: #ff5500; } "+ 
"#tartu_buildings [ fitness <= "+m[14]+"] {    polygon-fill: #ff8000; }  "+
"#tartu_buildings [ fitness <= "+m[13]+"] {    polygon-fill: #ffaa00; } "+
"#tartu_buildings [ fitness <= "+m[12]+"] {    polygon-fill: #ffd500; } "+
"#tartu_buildings [ fitness <= "+m[11]+"] {    polygon-fill: #ffff00; } "+
"#tartu_buildings [ fitness <= "+m[10]+"] {    polygon-fill: #d5ff00; } "+
"#tartu_buildings [ fitness <= "+m[9]+"] {    polygon-fill: #aaff00; } "+
"#tartu_buildings [ fitness <= "+m[8]+"] {    polygon-fill: #80ff00; } "+
"#tartu_buildings [ fitness <= "+m[7]+"] {    polygon-fill: #55ff00; } "+
"#tartu_buildings [ fitness <= "+m[6]+"] {    polygon-fill: #00ff00; } "+
"#tartu_buildings [ fitness <= "+m[5]+"] {    polygon-fill: #00ffff; } "+
"#tartu_buildings [ fitness <= "+m[4]+"] {    polygon-fill: #00d5ff; } "+
"#tartu_buildings [ fitness <= "+m[3]+"] {    polygon-fill: #00aaff; } "+
"#tartu_buildings [ fitness <= "+m[2]+"] {    polygon-fill: #0080ff; } "+
"#tartu_buildings [ fitness <= "+m[1]+"] {    polygon-fill: #0055ff; } "+
"#tartu_buildings [ fitness <= "+m[0]+"] {    polygon-fill: #0000ff; }      ";
}

function generateCSS(){
m = [];
for (var i = 1; i <= 18; i++) {
//console.log(segment(fitness,i));
m.push(segment(fitness,i));
}


css = "#tartu_buildings{   polygon-fill: #ff0000;   polygon-opacity: 0.7;   line-color: #FFF;   line-width: 0.5;   line-opacity: 1; }  "+
"#tartu_buildings [ fitness <= "+m[17]+"] {    polygon-fill: #ff0000; }  "+
"#tartu_buildings [ fitness <= "+m[16]+"] {    polygon-fill: #ff2b00; }  "+
"#tartu_buildings [ fitness <= "+m[15]+"] {    polygon-fill: #ff5500; } "+ 
"#tartu_buildings [ fitness <= "+m[14]+"] {    polygon-fill: #ff8000; }  "+
"#tartu_buildings [ fitness <= "+m[13]+"] {    polygon-fill: #ffaa00; } "+
"#tartu_buildings [ fitness <= "+m[12]+"] {    polygon-fill: #ffd500; } "+
"#tartu_buildings [ fitness <= "+m[11]+"] {    polygon-fill: #ffff00; } "+
"#tartu_buildings [ fitness <= "+m[10]+"] {    polygon-fill: #d5ff00; } "+
"#tartu_buildings [ fitness <= "+m[9]+"] {    polygon-fill: #aaff00; } "+
"#tartu_buildings [ fitness <= "+m[8]+"] {    polygon-fill: #80ff00; } "+
"#tartu_buildings [ fitness <= "+m[7]+"] {    polygon-fill: #55ff00; } "+
"#tartu_buildings [ fitness <= "+m[6]+"] {    polygon-fill: #00ff00; } "+
"#tartu_buildings [ fitness <= "+m[5]+"] {    polygon-fill: #00ffff; } "+
"#tartu_buildings [ fitness <= "+m[4]+"] {    polygon-fill: #00d5ff; } "+
"#tartu_buildings [ fitness <= "+m[3]+"] {    polygon-fill: #00aaff; } "+
"#tartu_buildings [ fitness <= "+m[2]+"] {    polygon-fill: #0080ff; } "+
"#tartu_buildings [ fitness <= "+m[1]+"] {    polygon-fill: #0055ff; } "+
"#tartu_buildings [ fitness <= "+m[0]+"] {    polygon-fill: #0000ff; }      ";
}

function getFitness(){
        var sql_fitness = "SELECT "+sql_list+" as fitness FROM "+sql_table+" WHERE "+area+" = 1  ORDER BY fitness ASC LIMIT "+dataset_size*(limit/100.0);
//        console.log(sql_fitness);
        var sql = new cartodb.SQL({ user: 'blue4world' });
        sql.execute(sql_fitness)
        .done(function(data) {
        fitness=processData(data);
        maximal=Math.max.apply(Math, fitness);
        minimal=Math.min.apply(Math, fitness);
        median=medianValue(fitness);
        q1=quantile1(fitness);
        q3=quantile3(fitness);
        

        if(legend_fixed == 1) {
                console.log("Using fixed scale");
                generateCSSfixed();
        }
        else {
                console.log("Using ventile scale");
                generateCSS();
        }

        drawMap();
        plotHistogram();
        plotBoxGraph();
        drawExpertInfo();
        })
        .error(function(errors) {
        console.log("errors:" + errors);
        })

}

function calculateDatasetSize(){
if(area=="area_all"){
dataset_size=area_all_size;
}
if(area=="area_2") {
dataset_size=area_2_size;
}
if(area=="area_1"){
dataset_size=area_1_size;
}

}

function drawMap(){
sql_q = "SELECT *,"+sql_list+"  AS fitness FROM "+sql_table+" WHERE "+area+" = 1 ORDER BY fitness ASC LIMIT "+dataset_size*(limit/100.0);
console.log(sql_q);

var layerUrl = 'http://blue4world.cartodb.com/api/v2/viz/415fd9c6-9943-11e5-8d0a-0e787de82d45/viz.json';

// change the query for the first layer
var subLayerOptions = {
sql: sql_q,
cartocss: css,
}

cartodb.createLayer(map, layerUrl)
.addTo(map)
.on('done', function(layer) {
    if(search_bar == 0) {
    var v = cdb.vis.Overlay.create('search', map.viz, {})
    v.show();
    $('#cartodb-map').append(v.render().el);
     search_bar=1;
     }
    if(info_window == 0) {
          cdb.vis.Vis.addInfowindow(map, layer.getSubLayer(0), ['fitness']);
        info_window=1;
        }
  })
.on('done', function(layer) {
  layer.getSubLayer(0).set(subLayerOptions);
}).on('error', function() {
  //log the error
});

}

function init(){
if(first_run==0){
if(map){
map_center=map.getCenter();
map_zoom=map.getZoom();
}
}
$.getScript("functions.js", function(){});
if(first_run==1){
calculateDataFromBasic();
first_run=0;
}
calculateDatasetSize();
calculateSqlList();
if (map) {
  console.log("Redwaring map...");
  map.remove();
} 
        // initiate leaflet map
        map = new L.Map('cartodb-map', { 
        center: map_center,
        zoom: map_zoom,
        })

        L.tileLayer('https://dnv9my2eseobd.cloudfront.net/v3/cartodb.map-4xtxp73f/{z}/{x}/{y}.png', {
         attribution: ' <a href="mailto:mikulas.muron@gmail.com" target="_top"><b>Muron, Sevcik</b></a>,<a href="https://bitbucket.org/blue4world/tartu-heatmap" target="_top"><b>Source code</b></a>'
        }).addTo(map);

        getFitness();


}  





